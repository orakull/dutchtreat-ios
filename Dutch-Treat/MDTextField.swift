//
//  MDTextField.swift
//  TestMaterialDesign
//
//  Created by Руслан Ольховка on 02.04.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import UIKit
import Material

@IBDesignable
class MDTextField: TextField {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		prepare()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		prepare()
	}
	
	func prepare() {
		backgroundColor = UIColor.clearColor()
		placeholderTextColor = MaterialColor.grey.base
		font = RobotoFont.regularWithSize(16)
		borderStyle = .None
		tintColor = MaterialColor.green.darken1
		bottomBorderColor = MaterialColor.grey.base
		
//		titleLabelAnimationDistance = 0
		titleLabel = UILabel()
		titleLabel!.font = RobotoFont.mediumWithSize(12)
		titleLabelColor = MaterialColor.grey.base
		titleLabelActiveColor = tintColor
		
		let image: UIImage? = MaterialIcon.close
		
		let clearBtn: FlatButton = FlatButton()
		clearBtn.pulseColor = MaterialColor.grey.base
		clearBtn.pulseScale = false
		clearBtn.tintColor = MaterialColor.grey.base
		clearBtn.setImage(image, forState: .Normal)
		clearBtn.setImage(image, forState: .Highlighted)
		
		clearButton = clearBtn
	}

}
