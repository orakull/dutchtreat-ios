//
//  MDFlatButton.swift
//  Dutch-Treat
//
//  Created by Руслан Ольховка on 03.04.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import UIKit
import Material

class MDFlatButton: FlatButton {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		prepare()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		prepare()
	}
	
	func prepare() {
		setTitleColor(MaterialColor.green.darken1, forState: .Normal)
		pulseColor = MaterialColor.green.darken1
		titleLabel!.font = RobotoFont.regular
	}
	
    /*
    //  override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
