//
//  LoginController.swift
//  Dutch-Treat
//
//  Created by Руслан Ольховка on 29.03.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

	@IBOutlet weak var loginTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
	@IBAction func enter() {
		let login = loginTextField.text!
		let password = passwordTextField.text!
		Gateway.login(login, password: password) {json in
//			Gateway.getEvents { json in
//				print("succes getEvents")
////				print(json)
//			}
			print("success login")
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
