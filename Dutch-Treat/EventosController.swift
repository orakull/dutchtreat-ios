//
//  EventosController.swift
//  Dutch-Treat
//
//  Created by Руслан Ольховка on 14.04.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import UIKit

struct Evento {
	var id: Int
	var name: String
}

class EventosController: UITableViewController {
	
	var eventos = [Evento]()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		refreshControl?.addTarget(self, action: #selector(handleRefresh), forControlEvents: .ValueChanged)
		
		getEventos()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
	
	func handleRefresh(refreshControl: UIRefreshControl) {
		getEventos()
	}
	
	func getEventos() {
		Gateway.getEvents({
			self.refreshControl!.endRefreshing()
			return true
		}) { (json) in
			
			var eventos = [Evento]()
			for (_, eventoJson) in json {
				let evento = Evento(
					id: eventoJson["id"].intValue,
					name: eventoJson["name"].stringValue
				)
				eventos.append(evento)
			}
			
			self.eventos = eventos
			dispatch_async(dispatch_get_main_queue(),{
				self.tableView.reloadData()
			});
			
			self.refreshControl!.endRefreshing()
			
		}
	}

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return eventos.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("eventoCell", forIndexPath: indexPath)
		
		let evento = eventos[indexPath.row]
		cell.textLabel?.text = evento.name
		cell.detailTextLabel?.text = "id: \(evento.id)"

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
