//
//  Gateway.swift
//  Dutch-Treat
//
//  Created by Руслан Ольховка on 29.03.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import Foundation
import SwiftyJSON

class Gateway
{
	static let hostURL = NSURL(string: "http://localhost:8080/dutch-treat/app/api/")!
//	static let hostURL = NSURL(string: "http://localhost:777/dutch-treat/app/api/")!
//	static let hostURL = NSURL(string: "http://semki.de1mos.net/dutch-treat/app/")!
	
	// MARK: - API Methods
	
	class func login(login: String, password: String,
	                 faultHandler: () -> Bool = { true },
	                 onComplete: () -> Void) {
		
		let url = NSURL(string: "auth/login", relativeToURL: hostURL)!
		let request = NSMutableURLRequest(URL: url)
		request.setValue("login", forHTTPHeaderField: "myRequestType")
		request.HTTPMethod = "POST"
		request.HTTPBody = "username=\(login)&password=\(password)".dataUsingEncoding(NSUTF8StringEncoding)
		
		baseRequest(request, faultHandler: {
			Params.Instance.user = nil
			return faultHandler()
		}) { _ in
			Params.Instance.user = (login, password)
			hideLoginVC()
			onComplete()
		}
	}
	
	class func getEvents(faultHandler: () -> Bool = { true },
	                     onComplete: (JSON) -> Void) {
		let url = NSURL(string: "eventos", relativeToURL: hostURL)!
		let request = NSMutableURLRequest(URL: url)
		request.HTTPMethod = "GET"
		baseRequest(request, faultHandler: faultHandler, completionHandler: onComplete)
	}
	
	// MARK: - base
	
	private class func baseRequest(request: NSMutableURLRequest,
	                               faultHandler: () -> Bool = { true },
	                               completionHandler: (JSON) -> Void) {
		print("request:", request.URL)
		
		func onFault(message: String, title: String? = nil) {
			if faultHandler() {
				ShowAlert(message, title: title)
			}
		}
		
		let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
		configuration.requestCachePolicy = .ReloadIgnoringLocalCacheData
		let session = NSURLSession(configuration: configuration)
		
		let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
			if let response = response as? NSHTTPURLResponse {
				switch response.statusCode {
				case 401:
					var isFromLogin = false
					if let myRequestType = request.valueForHTTPHeaderField("myRequestType") where myRequestType == "login" {
						isFromLogin = true
					}
					if let user = Params.Instance.user where !isFromLogin {
						login(user.login, password: user.password, faultHandler: faultHandler) {
							baseRequest(request, faultHandler: faultHandler, completionHandler: completionHandler)
						}
					} else {
						if loginVC == nil {
							showLoginVC {
								baseRequest(request, faultHandler: faultHandler, completionHandler: completionHandler)
							}
						} else {
							onFault("Проверьте пароль и попробуйте еще раз.", title: "Ошибка входа.")
						}
					}
				case 200:
					if let data = data {
						let json = JSON(data: data)
						completionHandler(json)
					}
				default:
					print("what is it?")
					print("response:", response)
					let str = NSHTTPURLResponse.localizedStringForStatusCode(response.statusCode)
					onFault(str, title: response.statusCode.description)
				}
			}
			else {
				onFault(error?.localizedDescription ?? "Неизвестная ошибка")
			}
		}
		task.resume()
	}
	
	// MARK: - Login view controller
	
	private static var loginVC: UIViewController?
	private static var loginHandler: (() -> Void)?
	
	private class func showLoginVC(loginHandler: (() -> Void)? = nil) {
		let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
		loginVC = mainStoryboard.instantiateViewControllerWithIdentifier("LoginVC")
		self.loginHandler = loginHandler
		
		if let rootVC = UIApplication.sharedApplication().delegate?.window??.rootViewController {
			rootVC.presentViewController(loginVC!, animated: true, completion: nil)
		}
	}
	
	private class func hideLoginVC() {
		guard let loginVC = loginVC else { return }
		if let loginHandler = loginHandler {
			loginHandler()
		}
		
		loginVC.dismissViewControllerAnimated(true) {
			self.loginVC = nil
		}
	}
	
}

// MARK: - Common goods

func ShowAlert(message: String?, title: String? = nil) {
	dispatch_async(dispatch_get_main_queue()) {
		if let rootVC = UIApplication.sharedApplication().delegate?.window??.rootViewController {
			let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
			let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
			alert.addAction(action)
			(rootVC.presentedViewController ?? rootVC).presentViewController(alert, animated: true, completion: nil)
		}
	}
}